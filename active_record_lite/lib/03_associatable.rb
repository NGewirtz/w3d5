require_relative '02_searchable'
require 'active_support/inflector'
require 'byebug'

# Phase IIIa
class AssocOptions
  attr_accessor(
    :foreign_key,
    :class_name,
    :primary_key
  )

  def model_class
    self.class_name.constantize
  end

  def table_name
    self.model_class.table_name
  end
end

class BelongsToOptions < AssocOptions
  def initialize(name, options = {})
    opts_hash = {
      foreign_key: "#{name}_id".to_sym,
      class_name: name.to_s.singularize.capitalize,
      primary_key: :id
    }.merge(options)
    opts_hash.each do |attr_name, val|
      attr_name = "#{attr_name.to_s}=".to_sym
      self.send(attr_name, val)
    end
  end
end

class HasManyOptions < AssocOptions
  def initialize(name, self_class_name, options = {})
    opts_hash = {
      foreign_key: "#{self_class_name}_id".downcase.to_sym,
      class_name: name.to_s.singularize.capitalize,
      primary_key: :id
    }.merge(options)
    # opts_hash.each do |attr_name, val|
    #   attr_name = "#{attr_name.to_s}=".to_sym
    #   self.send(attr_name, val)
    # end
    @foreign_key = opts_hash[:foreign_key]
    @class_name = opts_hash[:class_name]
    @primary_key = opts_hash[:primary_key]
  end
end

module Associatable
  # Phase IIIb
  def belongs_to(name, options = {})
    # ...
    options = BelongsToOptions.new(name, options)

    define_method(name) do
      foreign_key = options.send(:foreign_key)
      model_class = options.send(:model_class)
      model_class.where(id: self.send(foreign_key)).first
    end

  end

  def has_many(name, options = {})
    # ...
    options = HasManyOptions.new(name, self.to_s.underscore, options)

    define_method(name) do
      # foreign_key = options.send(:foreign_key)
      # model_class = options.send(:model_class)
      options.model_class.where(options.foreign_key => self.id )
    end

  end

  def assoc_options
    # Wait to implement this in Phase IVa. Modify `belongs_to`, too.
  end
end

class SQLObject
  # Mixin Associatable here...
  extend Associatable
end
