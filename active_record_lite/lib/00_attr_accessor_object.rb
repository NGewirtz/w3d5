class AttrAccessorObject
  def self.my_attr_accessor(*names)
    names.each do |name|

      define_method(name) do
        name = "@#{name}"
        instance_variable_get(name)
      end

      name_equals = "#{name}="
      sym_name = "@#{name}".to_sym
      define_method(name_equals) do |arg|
        instance_variable_set(sym_name, arg)
      end
    end

  end
end
