require_relative 'db_connection'
require_relative '01_sql_object'

module Searchable
  def where(params)
    # ...
    args = []
    where_line = params.map do |k, v|
      args << v
      "#{k} = ?"
    end
    where_line = where_line.join(" AND ")
    results = DBConnection.execute(<<-SQL, *args)
    SELECT
      *
    FROM
      #{self.table_name}
    WHERE
      #{where_line}
    SQL
    self.parse_all(results)
  end
end

class SQLObject
  # Mixin Searchable here...
  extend Searchable
end
