require_relative 'db_connection'
require 'active_support/inflector'
require 'byebug'
# NB: the attr_accessor we wrote in phase 0 is NOT used in the rest
# of this project. It was only a warm up.

class SQLObject
  def self.columns
    @cols ||= DBConnection.execute2(<<-SQL)[0].map(&:to_sym)
    SELECT
      *
    FROM
      #{self.table_name}
    SQL
    @cols
  end

  def self.finalize!
    self.columns.each do |col|
      define_method(col) do
        attributes[col]
      end

      equals_method = "#{col.to_s}=".to_sym
      define_method(equals_method) do |arg|
        attributes[col] = arg
      end

    end
  end

  def self.table_name=(table_name)
    # ...
    @table_name = table_name
  end

  def self.table_name
    @table_name ||= "#{self.to_s.tableize}"
    # ...
  end

  def self.all
    results = DBConnection.execute(<<-SQL)
    SELECT
      #{self.table_name}.*
    FROM
      #{self.table_name}
    SQL
    self.parse_all(results)
  end

  def self.parse_all(results)
    all_new_objects = []
    results.each do |result|
      all_new_objects << self.new(result)
    end
    all_new_objects
  end

  def self.find(id)
    # self.all.find { |obj| obj.id == id }

    result = DBConnection.execute(<<-SQL)
    SELECT
      *
    FROM
      #{self.table_name}
    WHERE
      id = #{id}
    SQL
    self.parse_all(result).first
  end

  def initialize(params = {})
    params.each do |attr_name, value|
      attr_name = attr_name.to_sym
      raise "unknown attribute '#{attr_name}'" unless self.class.columns.include?(attr_name)
      attr_name = "#{attr_name.to_s}=".to_sym
      self.send(attr_name, value)
    end

  end
    # ...
    # raise "unknown attribute '#{k}'" unless self.columns.include?(k)
    # method_to_define = "#{k}="
    # class_name = self.class
    # define_method(method_to_define) do |arg|
    #   instance_variable_set(k.to_sym, arg)
    # end

  def attributes
    @attributes ||= {}
  end

  def attribute_values
    self.class.columns.map { |col| self.send(col) }
    # ...
  end

  def insert
    col_names = self.class.columns.join(" ,")
    question_marks = (["?"] * self.class.columns.length).join(", ")
    args = self.attribute_values
    DBConnection.execute(<<-SQL, *args)
      INSERT INTO
        #{self.class.table_name} (#{col_names})
      VALUES
        (#{question_marks})

    SQL
    self.id = DBConnection.last_insert_row_id
    # ...
  end

  def update
    set = self.class.columns.map { |col_name| "#{col_name} = ?"}
    set = set.join(" ,")
    args = self.attribute_values
    DBConnection.execute(<<-SQL, *args)
      UPDATE
        #{self.class.table_name}
      SET
        #{set}
      WHERE
        id = #{self.id}
    SQL

    # ...
  end

  def save
    if self.id.nil?
      self.insert
    else
      self.update
    end
  end
end
